package bll;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import dataAcces.ProductDAO;
import model.Product;

public class ProductBLL {
	
	public Product findProductByName(String name) {
		Product p = ProductDAO.findByName(name);
		if (p == null) {
			throw new NoSuchElementException("The product: " + name + " was not found!");
		}
		return p;
	}
	
	public int insertProduct(Product p) {
		return ProductDAO.insert(p);
	}
	
	public int deleteProduct(int id){
		return ProductDAO.delete(id);
	}
	
	public ArrayList<Product> selectAllProducts(){
		return ProductDAO.select();
	}
	
	public int updateProduct(int id, int price, int availableQuantity){
		return ProductDAO.update(id, price, availableQuantity);
	}
}
