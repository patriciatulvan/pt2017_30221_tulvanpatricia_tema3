package bll;

import java.util.NoSuchElementException;

import dataAcces.OrdersDAO;
import model.Orders;

public class OrdersBLL {
	public Orders findProductById(int idOrder) {
		Orders o = OrdersDAO.findById(idOrder);
		if (o == null) {
			throw new NoSuchElementException("The order with id =" + idOrder + " was not found!");
		}
		return o;
	}
	
	public int insertOrder(Orders o) {
		return OrdersDAO.insert(o);
	}
	
	public int selectAllOrders(){
		return OrdersDAO.select();
	}
	
	public int listAllOrders(){
		return OrdersDAO.listAll();
	}
}
